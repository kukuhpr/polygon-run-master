# polygon-run-master
this project purpose to my Game Programming programe

## Rules
this game seperate by 2 stage, stage 1 `Forest` and stage 2 `Arabian Night`
the main charcter is `A Ball` which you can control it using keyboard
the **`Goals`** for this game is `Collect All Coin` which distributed on whole stage
if you have finished collect all coin go find `Red Flag` to go to next Stage.

another rules :
1. don't fall out the stage it can reduce your health
2. dont't hit cactus it also can reduce your health
3. you only have 3 life heart on start game
4. you can add heart by collect heart piece on whole stage

## Control
`ArrowUp | ▲` - Go Forward

`ArrowDown | ▼` - Go Backward

`ArrowLeft | ◄` - Turn Left

`ArrowRight | ►` - Turn Right

`Space` - Jump

## Quick Play
open `main` folder on this repository, find `Polygon Run.exe` to quick play
or you can download the main file [here](https://drive.google.com/file/d/1Olvgdrb7gQ2IhokFq0IOp-ZbNqSp21kA/view)
